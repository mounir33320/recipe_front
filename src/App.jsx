import {Navbar} from "./components/Navbar.jsx";
import {Outlet, RouterProvider, useNavigation} from "react-router-dom";
import {router} from "./router.jsx";
import {AuthContextProvider} from "./contexts/AuthContext.jsx";
import '@fortawesome/fontawesome-free/css/all.css';
import {Loader} from "./components/Loader/Loader.jsx";
import './App.css';
function App() {
    return (
        <>
            <AuthContextProvider>
                <RouterProvider router={router}/>
            </AuthContextProvider>
        </>
    )
}

export function Root() {
    const {state} = useNavigation();
    return (
        <>
            <Navbar/>
            <div className="container-fluid position-relative vh-100">
                {state === 'loading'&& <Loader/>}
                <Outlet/>
            </div>
        </>
    )
}


export default App
