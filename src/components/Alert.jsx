import React, { useState, useEffect } from "react";

export function Alert({ type, message, deleteAfter = null }) {
    const [visible, setVisible] = useState(true);

    useEffect(() => {
        let timeoutId;

        if (deleteAfter) {
            timeoutId = setTimeout(() => {
                setVisible(false);
            }, deleteAfter);
        }

        return () => {
            clearTimeout(timeoutId);
        };
    }, [deleteAfter]);

    if (!['success', 'error', 'warning', 'info'].includes(type) || !visible) {
        return null;
    }

    return (
        <div className={`alert alert-${type} alert-dismissible text-center`}>
            {message}
            <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    );
}
