import {NavLink} from "react-router-dom";
import {useAuth} from "../hooks/useAuth.jsx";
import {useAccount} from "../hooks/useAccount.js";

export function Navbar() {
    const {isAuth, user} = useAuth();
    const getAuthMenu = () => {
        return (
            <>
                <NavLink className="nav-link" to="/recipes">Recettes</NavLink>
                <NavLink className="nav-link" to="/ingredients">Ingrédients</NavLink>
                <NavLink className="nav-link" to="/categories">Catégories</NavLink>
                <NavLink className="nav-link" to="/shopping-items">Liste de course</NavLink>
            </>
        )
    }


    return (
        <nav className="navbar navbar-expand-lg bg-dark bg-gradient" data-bs-theme="dark">
            <div className="container-fluid">
                <NavLink className="navbar-brand" to="/">Accueil</NavLink>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div className="navbar-nav">

                        {isAuth
                            ?
                            getAuthMenu()
                            :
                            <NavLink className="nav-link" to="/login">Se connecter</NavLink>
                        }
                    </div>
                </div>
            </div>
        </nav>
    )
}
