import {forwardRef, useEffect, useId, useImperativeHandle, useRef, useState} from "react";
import TomSelect from "tom-select";
import "tom-select/dist/css/tom-select.bootstrap5.css";


export function Typeahead({list, label, name, customSettings = null, items = null}) {
    const id = useId()
    const inputRef = useRef(null);
    const [tomSelect, setTomSelect] = useState(null)

    const handleSetTomSelect = (tomSelect) => {
        setTomSelect(tomSelect);
    }


    useEffect(() => {
        const settings = {...getDefaultTomSelectSettings(), ...customSettings, items};
        const options = list.map(element => ({
            value: element,
            text: element
        }));
        let updatedSettings = { ...settings, options };
        updatedSettings = {...updatedSettings}

        const tomSelect = new TomSelect(inputRef.current, updatedSettings);
        handleSetTomSelect(tomSelect);

        return () => {
            tomSelect.destroy();
        };
    }, []);

    return (
        <>
            <label htmlFor={id}>{label}</label>
            <input name={name} ref={inputRef} type="text" className="form-control"/>
        </>
    )
}

function getDefaultTomSelectSettings() {
    return {
        persist: true,
        createOnBlur: true,
        create: true,
        render: {
            option_create: (data, escape) => {
                return '<div class="create">Ajouter <strong>' + escape(data.input) + '</strong>&hellip;</div>';
            },
            no_results: function(data, escape){
                return '';
            },
        }
    }
}
