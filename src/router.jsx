import {createBrowserRouter, Navigate, redirect} from "react-router-dom";
import {RecipeList} from "././pages/recipes/RecipeList.jsx";
import Login from "./pages/Login.jsx";
import {Root} from "./App.jsx";
import {PageError} from "./pages/PageError.jsx";
import {Recipe} from "./pages/recipes/Recipe.jsx";
import {RecipeAdd} from "./pages/recipes/RecipeAdd.jsx";
import {DayList} from "./pages/days/DayList.jsx";
import {getAllRecipes, getRecipeById} from "./functions/api/recipe.js";
import {getAllIngredients} from "./functions/api/ingredient.js";
import {getAllDays} from "./functions/api/day.js";
import {ShoppingItemList} from "./pages/shopping-items/ShoppingItemList.jsx";
import {getAllShoppingItems} from "./functions/api/shoppingList.js";
import {IngredientList} from "./pages/ingredients/IngredientList.jsx";
import {CategoryList} from "./pages/categories/CategoryList.jsx";
import {getAllCategories} from "./functions/api/category.js";

export const router = createBrowserRouter([
    {
        path: '/',
        element: <Root/>,
        errorElement: <PageError/>,
        children: [
            {
                path: '',
                element: <DayList/>,
                loader: async () => {
                    const responseDays = await getAllDays();
                    const responseRecipes = await getAllRecipes();
                    return { daysData: responseDays.data, recipesData: responseRecipes.data};
                }
            },
            {
                path: 'recipes',
                children: [
                    {
                        path: '',
                        element: <RecipeList/>,
                        loader: async () => {
                            const response = await getAllRecipes();
                            return response.data;
                        }
                    },
                    {
                        path: 'add',
                        element: <RecipeAdd/>,
                        loader: async () => {
                            const ingredientsResponse = await getAllIngredients()
                            const categoriesResponse = await getAllCategories()

                            return {ingredients:ingredientsResponse.data, categories:categoriesResponse.data};
                        }
                    },
                    {
                        path: ':id',
                        element: <Recipe/>,
                        loader: async ({ params }) => {
                            const response = await getRecipeById(params.id)
                            return response.data;
                        }
                    }
                ]
            },
            {
                path: 'ingredients',
                children: [
                    {
                        path: '',
                        element: <IngredientList/>,
                        loader: async () => {
                            const ingredientsResponse = await getAllIngredients()
                            return ingredientsResponse.data;
                        }
                    }
                ]
            },
            {
                path: 'categories',
                children: [
                    {
                        path: '',
                        element: <CategoryList/>,
                        loader: async () => {
                            const categoriesResponse = await getAllCategories()
                            return categoriesResponse.data;
                        }
                    }
                ]
            },
            {
                path: 'shopping-items',
                element: <ShoppingItemList/>,
                loader: async () => {
                   const response = await getAllShoppingItems();
                   return response.data;
                }
            },
            {
                path: 'login',
                element: <Login/>,
                loader: () => {
                    if (localStorage.getItem('currentUser')) {
                        return redirect('/');
                    }
                    return null;
                }
            },
            {
                path: '*',element: <Navigate to='/' replace/>
            }
        ]
    }
])

export class ApiError extends Error {

    constructor(status, message) {
        super();

        this.status = status;
        this.message = message;
    }
}
