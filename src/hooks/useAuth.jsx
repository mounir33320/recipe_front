import {apiFetch} from "../functions/apiFetch.js";
import {useContext, useEffect, useState} from "react";
import {AuthContext} from "../contexts/AuthContext.jsx";
import getApiUrl from "../functions/getApiUrl.js";
import {decrypt, encrypt} from "../functions/crypt.js";
import {getToken, getUserByToken, hasToken, setRefreshToken, setToken} from "../functions/tokenAuth.js";


export function useAuth() {
    const {isAuth, setIsAuth} = useContext(AuthContext);
    const [error, setError] = useState('');
    const [user, setUser] = useState(null);

    useEffect(() => {
        if (hasToken()) {
            setIsAuth(true);
            setUser(getUserByToken());
        } else {
            setUser(null)
            setIsAuth(false)
        }
    }, [])

    const login = async (username, password) => {
        const url = `${getApiUrl()}/api/login_check`;
        const options = {
            method: 'POST',
            body: JSON.stringify({username, password})
        }

        const response = await apiFetch(url, options);

        if (response.token) {
            setToken(response.token);
            setRefreshToken(response.refresh_token);

            setIsAuth(true);
            setUser(getUserByToken())
        } else {
            setError(response.error);
            setIsAuth(false);
            setUser(null)
        }
    }

    return {isAuth, setIsAuth, login, user, error};
}
