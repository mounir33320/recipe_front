import {useState} from "react";

export function useApiRequest() {
    const [data, setData] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null)

    const sendRequest = async (callback, data = null) => {
        setLoading(true);

        try {
            const response = data !== null ? await callback(data) : await callback();
            setData(response.data);
        } catch (error) {
            setError(error)
        } finally {
            setLoading(false)
        }
    }


    return {loading, sendRequest, data, error}

}
