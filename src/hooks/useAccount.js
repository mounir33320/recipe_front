import {useAuth} from "./useAuth.jsx";
import {decrypt} from "../functions/crypt.js";

export function useAccount() {
    const {isAuth} = useAuth()

    if (!isAuth) {
        return null;
    }

    const encryptedUser = localStorage.getItem('currentUser')
    return JSON.parse(decrypt(encryptedUser));
}
