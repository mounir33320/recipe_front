import {NavLink, useLoaderData} from "react-router-dom";
import {Typeahead} from "../../components/Typeahead.jsx";
import {useEffect, useState} from "react";
import {apiFetch} from "../../functions/apiFetch.js";
import getApiUrl from "../../functions/getApiUrl.js";
// import {usePutDay} from "../../hooks/usePutDay.jsx";
import {Loader} from "../../components/Loader/Loader.jsx";
import {useApiRequest} from "../../hooks/useApiRequest.jsx";
import {updateDay} from "../../functions/api/day.js";

export function DayList() {
    const {daysData, recipesData} = useLoaderData()

    const daysCards = daysData.map(day => {
        return <div key={day.id} className="col mb-2">
            <DayCard day={day} recipesData={recipesData}/>
        </div>
    })


    return (
        <section className="mt-3">
            <div className="row row-cols-lg-5 row-cols-md-2 row-cols-1">
                {daysCards}
            </div>
        </section>
    )
}

function DayCard({day, recipesData}) {
    const [currentDay, setCurrentDay] = useState(day);

    const {loading, sendRequest, data} = useApiRequest();
    const [showLunchInput, setShowLunchInput] = useState(false);
    const [showDinnerInput, setShowDinnerInput] = useState(false);


    const handleUpdateCurrentDay = () => {
        if (data) {
            setCurrentDay(data);
        }
    }

    useEffect(() => {
        handleUpdateCurrentDay()
    }, [data]);

    const lunchRecipes = currentDay.recipes.filter(recipe => recipe.type === 'lunch')
    const dinnerRecipes = currentDay.recipes.filter(recipe => recipe.type === 'dinner')

    const recipesTypeaheadList = recipesData.map(recipe => recipe.title)

    const handleClick = (e) => {
        if (e.currentTarget.id === 'lunch-button') {
            setShowLunchInput(v => !v);
        } else {
            setShowDinnerInput(v => !v)
        }
    }

    const hideInputs = () => {
        setShowDinnerInput(false)
        setShowLunchInput(false)
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const formData = new FormData(e.currentTarget);
        const updatedDay = {...currentDay};

        for (const [key, value] of formData) {
            const recipesAdded = recipesData.filter(recipe => {
                const titles = value.split(',');
                return titles.includes(recipe.title);
            })

            for (let recipeAdded of recipesAdded) {
                const foundRecipe = updatedDay.recipes.find(currentRecipe => currentRecipe.id === recipeAdded.id && currentRecipe.type === key);
                if (!foundRecipe) {
                    updatedDay.recipes.push({id: recipeAdded.id, type: key})
                }
            }
        }

        await sendRequest(updateDay, updatedDay);
        hideInputs()
    }

    const onDelete = async (e) => {
        if (confirm('Êtes-vous sûr de réaliser cette action ?')) {

            const recipeIDToDelete = +e.currentTarget.dataset.id;
            const mealType = e.currentTarget.dataset.meal;
            const updatedDay = {...currentDay};

            updatedDay.recipes = updatedDay.recipes.filter(recipe => {
                if (recipe.id !== recipeIDToDelete) {
                    return true
                }
                return recipe.id === recipeIDToDelete && recipe.type !== mealType;

            });

            await sendRequest(updateDay, updatedDay);
        }
    }

    const buildList = (recipes) => {
        if (recipes.length > 0) {
            return <ul className="list-group">
                {
                    recipes.map(recipe => (
                        <li className="list-group-item py-1 px-2 d-flex justify-content-between" key={recipe.id}>
                            <NavLink className="vertical w-100 d-flex link-underline link-underline-opacity-0 d-block" to={`/recipes/${recipe.id}`}>
                                {recipe.title}
                            </NavLink>
                            <span data-meal={recipe.type} data-id={recipe.id} onClick={onDelete} className="text-danger bg-gradient cursor-pointer">
                                <i className="fa-solid fa-trash"></i>
                            </span>
                        </li>
                        )
                    )
                }
            </ul>
        }
        return <p className="fst-italic text-center">Pas de recette</p>
    }

    return (
        <>
            {loading && <Loader/>}
            <div className="card">
                <h3 className="card-header text-center">{currentDay.name}</h3>
                <div className="card-body">
                    <h5 className="bg-dark px-3 card-title fw-bold text-white d-flex justify-content-between align-items-center">
                        <div>MIDI</div>
                        <button id="lunch-button" onClick={handleClick} className="btn btn-unstyled text-success">
                            <i className="fa-solid fa-plus"></i>
                        </button>
                    </h5>

                    {showLunchInput &&
                        <form onSubmit={handleSubmit} className="mb-3">
                            <div className="input-group input-group-sm mb-3">
                                <Typeahead name="lunch" list={recipesTypeaheadList} customSettings={{create: false}}/>
                                <button className="btn btn-success" type="submit" id="button-addon2">+</button>
                            </div>
                        </form>}

                    {buildList(lunchRecipes)}

                    <h5 className="bg-dark px-3 mt-3 card-title fw-bold text-white d-flex justify-content-between align-items-center">
                        <div>SOIR</div>
                        <button id="dinner-button" onClick={handleClick} className="btn btn-unstyled text-success">
                            <i className="fa-solid fa-plus"></i>
                        </button>
                    </h5>
                    {showDinnerInput &&
                        <form onSubmit={handleSubmit} className="mb-3">
                            <div className="input-group input-group-sm mb-3">
                                <Typeahead name="dinner" list={recipesTypeaheadList} customSettings={{create: false}}/>
                                <button className="btn btn-success" type="submit" id="button-addon2">+</button>
                            </div>

                        </form>}
                    {buildList(dinnerRecipes)}
                </div>
            </div>
        </>

    )
}
