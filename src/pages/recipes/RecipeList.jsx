import {NavLink, useLoaderData} from "react-router-dom";
import {useState} from "react";

export function RecipeList() {
    const recipes = useLoaderData();
    const recipeCardList = [];
    const [search, setSearch] = useState('');

    const handleSearch = (e) => {
        setSearch(e.currentTarget.value);
    }

    const filteredRecipes = filterRecipes(search, recipes)

    for (const recipe of filteredRecipes) {
        const recipeCard = <RecipeCard key={recipe.id} recipe={recipe}/>
        recipeCardList.push(recipeCard)
    }

    return (
        <>
            <header className="d-flex justify-content-between align-items-end">
                <h1>Liste des recettes</h1>
                <div>
                    <NavLink to="add" className="btn btn-success">
                        <i className="fa-solid fa-plus"></i>
                    </NavLink>
                </div>
            </header>
            <hr/>

            <section>
                <div className="row">
                    <div className="col-md-2 col-12">
                        <form role="search" className="mb-4">
                            <input onChange={handleSearch} className="form-control" type="search" placeholder="Recherche..."/>
                        </form>
                    </div>
                </div>

                <div className="row">
                    {recipeCardList.length > 0 ? recipeCardList : <h3 className="text-center">Aucune recette</h3>}

                </div>
            </section>
        </>
    )
}

export function RecipeCard({recipe}) {
    const categories = [];
    for (const category of recipe.categories) {
        const categoryBadge = <span key={category.id} className="badge bg-primary me-1">{category.name}</span>
        categories.push(categoryBadge);
    }

    const ingredients = [];

    for (const ingredient of recipe.ingredients) {
        const ingredientItem = <span key={ingredient.id} className="badge bg-dark me-2">{ingredient.name}</span>
        ingredients.push(ingredientItem);
    }

    return (
        <>
            <div className="col-12 col-md-3">
                <NavLink style={{textDecoration: "none"}} to={recipe.id.toString()} className="card text-bg-secondary bg-gradient mb-3">
                    <div className="card-header">
                        {recipe.title} {categories}
                    </div>
                    <div className="card-body">
                            {ingredients}
                        <p className="card-text">{recipe.description}</p>
                    </div>
                </NavLink>
            </div>
        </>
    )
}

const filterRecipes = (search, recipes) => {
    if (search) {
        search = search.trim().toLowerCase();
        return recipes.filter(recipe => {
            if (recipe.title.toLowerCase().includes(search)) {
                return true;
            }

            let inCategories = recipe.categories.some(category => {
                return category.name.toLowerCase().includes(search);
            })

            if (inCategories) {
                return true;
            }

            return recipe.ingredients.some(ingredient => {
                return ingredient.name.toLowerCase().includes(search);
            });
        })
    }
    return recipes;
}
