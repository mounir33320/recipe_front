import {useEffect, useRef} from "react";
import {Typeahead} from "../../components/Typeahead.jsx";
import {NavLink, useLoaderData} from "react-router-dom";
import {Loader} from "../../components/Loader/Loader.jsx";
import {Alert} from "../../components/Alert.jsx";
import {createRecipe} from "../../functions/api/recipe.js";
import {useApiRequest} from "../../hooks/useApiRequest.jsx";
import {buildDataToPush} from "./functions.js";

export function RecipeAdd() {

    const { ingredients, categories } = useLoaderData()
    const  {loading, sendRequest, data:recipeResponse, error } = useApiRequest();
    const formRef = useRef(null);

    const clearForm = (form) => {
        form.querySelectorAll('.tomselected').forEach(input => {
            input.tomselect.clear();
        })
        form.reset();
    }

    useEffect(() => {
        if (recipeResponse) {
            clearForm(formRef.current)
        }
    }, [recipeResponse]);

    const handleSubmit = async (e) => {
        e.preventDefault();
        const formData = new FormData(e.currentTarget);
        let recipe = {
            title: formData.get('title'),
            comment: formData.get('comment')
        }

        const ingredientsToPush = buildDataToPush(formData.get('ingredients'), ingredients);
        const categoriesToPush = buildDataToPush(formData.get('categories'), categories);

        recipe = {
            ...recipe,
            ingredients: ingredientsToPush,
            categories: categoriesToPush
        }
        await sendRequest(createRecipe, recipe);
    }

    return (
        <div className="row">
            <div className="col-12">
                {loading && <Loader/>}
                <h1>Ajouter une recette</h1>
                <hr/>
                <NavLink to="/recipes" className="btn btn-sm btn-secondary">
                    <i className="fa-solid fa-circle-arrow-left"></i> Retour
                </NavLink>

                {recipeResponse && <Alert type="success" message="Recette ajoutée avec succès" deleteAfter="8000"/>}
                {error && <Alert type="danger" message={error} />}

                <form ref={formRef} method="POST" onSubmit={handleSubmit}>
                    <div className="row align-items-end">
                        <div className="col-md-6 col-12">
                            <label htmlFor="title">Nom</label>
                            <input className="form-control" type="text" name="title" id="title"/>
                        </div>

                        <div className="col-md-3 col-12">
                            <Typeahead name="ingredients" label="Ingrédients" list={ingredients.map(ingredient => ingredient.name)}/>
                        </div>

                        <div className="col-md-3 col-12">
                            <Typeahead name="categories" label="Catégories" list={categories.map(category => category.name)}/>
                        </div>

                        <div className="col-12">
                            <label htmlFor="comment">Description</label>
                            <textarea name="comment" id="comment" className="form-control"></textarea>
                        </div>
                        <div className="col-md-2 mt-3">
                            <button className="btn btn-primary w-100" type="submit">Créer</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}


