import {useCallback, useEffect, useMemo, useRef, useState} from "react";
import {Typeahead} from "../../components/Typeahead.jsx";
import {Loader} from "../../components/Loader/Loader.jsx";
import {Alert} from "../../components/Alert.jsx";
import {useApiRequest} from "../../hooks/useApiRequest.jsx";
import {buildDataToPush} from "./functions.js";
import {getAllIngredients} from "../../functions/api/ingredient.js";
import {updateRecipe} from "../../functions/api/recipe.js";
import {getAllCategories} from "../../functions/api/category.js";

export function RecipeEdit({recipe, editionMode, setRecipe}) {

    const  {loading, sendRequest, data:recipeResponse, error } = useApiRequest();
    const formRef = useRef(null);
    const [ingredientsList, setIngredientsList] = useState(null)
    const [categoriesList, setCategoriesList] = useState(null)



    useEffect(() => {
        const setLists = async () => {
            let response = await getAllIngredients()
            setIngredientsList(response.data);

            response = await getAllCategories()
            setCategoriesList(response.data);

        }
        setLists().then();
    }, [])

    if (categoriesList === null && ingredientsList === null) {
        return null;
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const formData = new FormData(e.currentTarget);
        recipe = {
            ...recipe,
            title: formData.get('title'),
            comment: formData.get('comment')
        }

        const ingredientsToPush = buildDataToPush(formData.get('ingredients'), ingredientsList);
        const categoriesToPush = buildDataToPush(formData.get('categories'), categoriesList);

        recipe = {
            ...recipe,
            ingredients: ingredientsToPush,
            categories: categoriesToPush
        }
        await sendRequest(updateRecipe,recipe);
        setRecipe(recipe);
    }

    return (
        <div className="row">
            <div className="col-12">
                {loading && <Loader/>}
                <h1>Modifier la recette</h1>
                <hr/>
                <button onClick={() => editionMode(false)} className="btn btn-sm btn-secondary">
                    <i className="fa-solid fa-circle-arrow-left"></i> Retour
                </button>

                {recipeResponse && <Alert type="success" message="Recette modifiée avec succès" deleteAfter="38000"/>}
                {error && <Alert type="danger" message={error} />}

                <form ref={formRef} method="POST" onSubmit={handleSubmit}>
                    <div className="row align-items-end">
                        <div className="col-md-6 col-12">
                            <label htmlFor="title">Nom</label>
                            <input defaultValue={recipe.title} className="form-control" type="text" name="title" id="title"/>
                        </div>

                        <div className="col-md-3 col-12">
                            {ingredientsList &&
                                <Typeahead items={recipe.ingredients.map(ingredient => ingredient.name)} name="ingredients" label="Ingrédients" list={ingredientsList.map(ingredient => ingredient.name)}/>}

                        </div>

                        <div className="col-md-3 col-12">
                            {categoriesList &&
                                <Typeahead items={recipe.categories.map(category => category.name)} name="categories" label="Catégories" list={categoriesList.map(category => category.name)}/>}

                        </div>

                        <div className="col-12">
                            <label htmlFor="comment">Description</label>
                            <textarea defaultValue={recipe.comment} name="comment" id="comment" className="form-control"></textarea>
                        </div>
                        <div className="col-md-2 mt-3">
                            <button className="btn btn-primary w-100" type="submit">Modifier</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}
