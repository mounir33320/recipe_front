import {useLoaderData, useNavigate} from "react-router-dom";
import {useEffect, useState} from "react";
import {RecipeEdit} from "./RecipeEdit.jsx";
import bootstrap from "bootstrap/dist/js/bootstrap.bundle.js";
import "notyf/notyf.min.css"
import {showNotyf} from "../../functions/notyf.js";
import {createShoppingItem} from "../../functions/api/shoppingList.js";


export function Recipe() {
    const recipeLoaded = useLoaderData();
    const [recipe, setRecipe] = useState(recipeLoaded);
    const navigate = useNavigate();
    const [isEditionMode, setIsEditionMode] = useState(false);
    const [tooltipListArray, setTooltipListArray] = useState(null)

    useEffect(() => {
        const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
        const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))
        setTooltipListArray(tooltipList);
    }, []);


    let categoryBadges = [];
    if (recipe.categories.length > 0) {
        categoryBadges = recipe.categories.map(category => (<span key={category.id} className="badge bg-danger bg-gradient me-2">{category.name}</span>))
    }

    const addToShoppingList = async (e) => {
        e.preventDefault();
        const shoppingItem = {
            name: e.currentTarget.dataset.ingredient,
            isActive: true
        }

        try {
            const response = await createShoppingItem(shoppingItem);
            showNotyf('success', 'Ajouté à la liste !');


        } catch (error) {
            showNotyf('error', 'Une erreur est survenue...');
        }
    }

    let ingredientsListItems = [];
    if (recipe.ingredients) {
        ingredientsListItems = recipe.ingredients.map(ingredient => {
            return <li key={ingredient.id}>
                {ingredient.name}
                <a data-ingredient={ingredient.name} onClick={addToShoppingList} href="#" className="ms-2 text-success" data-bs-toggle="tooltip" data-bs-title="Ajouter à la liste de course">
                    <i className="fa-solid fa-plus"></i>
                </a>
            </li>
        });
    }

    if (isEditionMode) {
        return <RecipeEdit editionMode={setIsEditionMode} recipe={recipe} setRecipe={setRecipe}/>
    }

    return (
        <>
            <header className="d-flex align-items-center justify-content-between">
                <div>
                    <h1>{recipe.title}</h1>
                    {categoryBadges.length > 0 && <div>{categoryBadges}</div>}
                </div>

                <button onClick={() => setIsEditionMode(true)} className="btn btn-sm btn-warning">
                    <i className="fa-solid fa-pen-to-square"></i>
                </button>
            </header>
            <hr/>

            <button className="btn btn-sm btn-secondary" onClick={() => navigate(-1)}>
                <i className="fa-solid fa-circle-arrow-left"></i> Retour
            </button>

            <div className="container mt-4">
                <div className="card text-bg-light mb-3">
                    <h3 className="card-header">Ingrédients</h3>
                    {ingredientsListItems.length > 0
                        ?
                    <div className="card-body">
                        <ul>
                            {ingredientsListItems}
                        </ul>
                    </div>
                        :
                    <p>Aucun ingrédient</p>
                    }
                    <h3 className="card-header">Description</h3>
                    <div className="card-body">
                        <p style={{whiteSpace: 'pre-line'}} className="card-text">{recipe.comment.length > 0 ? recipe.comment : 'Pas de description'}</p>
                    </div>
                </div>
            </div>
        </>
    )
}
