export const buildDataToPush = (data, list) => {
    const dataArray = data.toString().split(',');

    const dataToPush = [];
    dataArray.forEach(item => {

        const existItem = list.find(listItem => {
            return listItem.name === item;
        })

        if (existItem === undefined) {
            dataToPush.push({id: null, name: item})
        } else {
            dataToPush.push(existItem);
        }
    })

    return dataToPush;
}
