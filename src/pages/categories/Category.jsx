import {useApiRequest} from "../../hooks/useApiRequest.jsx";
import {deleteCategory, updateCategory} from "../../functions/api/category.js";
import {showNotyf} from "../../functions/notyf.js";
import {useState} from "react";

export function Category({category, setIsLoading, setCategoriesToShow}) {

    const {loading, sendRequest, data, error} = useApiRequest()
    const [isEdit, setIsEdit] = useState(false);

    const onEdit = async (category) => {
        setIsEdit(true);
    }

    const onDelete = async (category) => {
        setIsLoading(true)
        await sendRequest(deleteCategory, category.id);
        setIsLoading(false)

        if (error) {
            showNotyf('error', error.toString())
        } else {
            showNotyf('success', 'Catégorie supprimée')
            setCategoriesToShow(categoryList => categoryList.filter(currentCategory => currentCategory.id !== category.id))
        }
    }

    return (
        <>
            <tr>
                <td>
                    {isEdit ? <CategoryEdit setCategoriesToShow={setCategoriesToShow} category={category} setIsEdit={setIsEdit} setIsLoading={setIsLoading}/> : category.name}
                </td>
                <td>
                    <div className="d-flex justify-content-center">
                        <button onClick={() => onEdit(category)} className="btn btn-sm btn-secondary me-2">
                            <i className="fa-solid fa-pen-to-square"></i>
                        </button>

                        <button onClick={() => onDelete(category)} className="btn btn-sm btn-danger">
                            <i className="fa-solid fa-trash"></i>
                        </button>
                    </div>
                </td>
            </tr>
        </>

    )
}

function CategoryEdit({category, setIsEdit, setIsLoading, setCategoriesToShow}) {

    const { sendRequest, error} = useApiRequest();

    const onCancel = () => {
        setIsEdit(false);
    }

    const onSubmit = async (e, category) => {
        e.preventDefault();
        const updatedCategory = { ...category, name: e.currentTarget.category.value }
        setIsLoading(true);
        await sendRequest(updateCategory, updatedCategory)
        setIsLoading(false)
        setIsEdit(false)

        if (error) {
            showNotyf('error', error.toString())
        } else {
            showNotyf('success', 'Catégorie modifiée')

            setCategoriesToShow(categoryList => {
                return categoryList.map(currentCategory => {
                    if (currentCategory.id === updatedCategory.id) {
                        return updatedCategory;
                    }
                    return currentCategory;
                })
            })
        }
    }

    return (
        <form onSubmit={(e) => onSubmit(e, category)}>
            <div className="input-group">
                <input name="category" type="text" className="form-control form-control-sm" defaultValue={category.name}/>
                <button type="submit" className="btn btn-sm btn-outline-success bg-gradient">
                    <i className="fa-solid fa-check"></i>
                </button>
                <button onClick={onCancel} type="button" className="btn btn-sm btn-outline-danger bg-gradient">
                    <i className="fa-solid fa-xmark"></i>
                </button>
            </div>
        </form>
    )
}
