import {useLoaderData} from "react-router-dom";
import {Category} from "./Category.jsx";
import {useState} from "react";
import {Loader} from "../../components/Loader/Loader.jsx";

export function CategoryList() {
    const categories = useLoaderData();

    let [categoriesToShow, setCategoriesToShow] = useState(categories);

    const categoriesComponents = [];
    const [isLoading, setIsLoading] = useState(false)
    const [search, setSearch] = useState('');

    const handleSearch = (e) => {
        setSearch(e.currentTarget.value);
    }

    categoriesToShow = filterCategories(search, categoriesToShow)

    for (let category of categoriesToShow) {
        let categoryComponent = <Category key={category.id} setCategoriesToShow={setCategoriesToShow} category={category} setIsLoading={setIsLoading}/>
        categoriesComponents.push(categoryComponent);
    }

    return (
        <>
            <header>
                <h1>Liste des catégories</h1>
            </header>
            <hr/>

            <div className="row">
                <div className="col-md-2 col-12">
                    <form role="search" className="mb-4">
                        <input onChange={handleSearch} className="form-control" type="search" placeholder="Recherche..."/>
                    </form>
                </div>
            </div>

            <section>
                {isLoading && <Loader/>}
                <table className="table">
                    <thead>
                        <tr>
                            <th>Catégorie</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {categoriesComponents}
                    </tbody>
                </table>
            </section>
        </>
    )
}

const filterCategories = (search, categories) => {
    if (search) {
        search = search.trim().toLowerCase();
        return categories.filter(categorie => {
            return categorie.name.toLowerCase().includes(search)
        })
    }
    return categories;
}
