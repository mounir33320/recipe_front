import {Navigate, useNavigate, useRouteError} from "react-router-dom";
import {Navbar} from "../components/Navbar.jsx";
import {ApiError} from "../router.jsx";
import {useAuth} from "../hooks/useAuth.jsx";
import {useEffect} from "react";

export function PageError() {
    const error = useRouteError()
    const navigate = useNavigate();
    const {isAuth, setIsAuth} = useAuth();

    useEffect(() => {
        if (error instanceof ApiError && error.status === 401) {
            localStorage.removeItem("currentUser");
            setIsAuth(false);
        }
    }, [isAuth]);


    if (error instanceof ApiError && error.status === 401) {
        return <Navigate  to='/login'/>
    }

    if (error instanceof ApiError && error.status === 403) {
        return <>
            <Navbar/>
            <h1>Vous n'avez pas accès à cette page...</h1>
        </>
    }

    console.log(error);

    return (
        <>
            <Navbar/>
            <h1>Une erreur est survenue...</h1>
        </>
    )
}
