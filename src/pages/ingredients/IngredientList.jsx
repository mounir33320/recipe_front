import {useLoaderData, useRouteLoaderData} from "react-router-dom";
import {Ingredient} from "./Ingredient.jsx";
import {useEffect, useState} from "react";
import {Loader} from "../../components/Loader/Loader.jsx";
import bootstrap from "bootstrap/dist/js/bootstrap.bundle.js";

export function IngredientList() {
    const ingredients = useLoaderData();
    let [ingredientsToShow, setIngredientsToShow] = useState(ingredients);
    const [isLoading, setIsLoading] = useState(false)
    const [tooltipListArray, setTooltipListArray] = useState(null)
    const [search, setSearch] = useState('');

    const handleSearch = (e) => {
        setSearch(e.currentTarget.value);
    }

    ingredientsToShow = filterIngredients(search, ingredientsToShow)

    const ingredientsComponents = [];

    useEffect(() => {
        const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
        const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))
        setTooltipListArray(tooltipList);
    }, []);

    for (let ingredient of ingredientsToShow) {
        let ingredientComponent = <Ingredient key={ingredient.id} setIngredientsToShow={setIngredientsToShow} ingredient={ingredient} setIsLoading={setIsLoading}/>
        ingredientsComponents.push(ingredientComponent);
    }

    return (
        <>
            <header>
                <h1>Liste des ingrédients</h1>
            </header>
            <hr/>

            <div className="row">
                <div className="col-md-2 col-12">
                    <form role="search" className="mb-4">
                        <input onChange={handleSearch} className="form-control" type="search" placeholder="Recherche..."/>
                    </form>
                </div>
            </div>

            <section>
                {isLoading && <Loader/>}
                <table className="table">
                    <thead>
                        <tr>
                            <th>Ingrédient</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {ingredientsComponents}
                    </tbody>
                </table>
            </section>
        </>
    )
}

const filterIngredients = (search, ingredients) => {
    if (search) {
        search = search.trim().toLowerCase();
        return ingredients.filter(ingredient => {
            return ingredient.name.toLowerCase().includes(search)
        })
    }
    return ingredients;
}
