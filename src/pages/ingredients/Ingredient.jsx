import {useApiRequest} from "../../hooks/useApiRequest.jsx";
import {Loader} from "../../components/Loader/Loader.jsx";
import {deleteIngredient, updateIngredient} from "../../functions/api/ingredient.js";
import {showNotyf} from "../../functions/notyf.js";
import {useState} from "react";
import {createShoppingItem} from "../../functions/api/shoppingList.js";

export function Ingredient({ingredient, setIsLoading, setIngredientsToShow}) {

    const {loading, sendRequest, data, error} = useApiRequest()
    const [isEdit, setIsEdit] = useState(false);

    const onEdit = async (ingredient) => {
        setIsEdit(true);
    }

    const onDelete = async (ingredient) => {
        setIsLoading(true)
        await sendRequest(deleteIngredient, ingredient.id);
        setIsLoading(false)

        if (error) {
            showNotyf('error', error.toString())
        } else {
            showNotyf('success', 'Ingrédient supprimé')
            setIngredientsToShow(ingredientList => ingredientList.filter(currentIngredient => currentIngredient.id !== ingredient.id))
        }
    }

    const addToShoppingList = async (ingredient) => {
        const shoppingItem = {
            name: ingredient.name,
            isActive: true
        }

        try {
            setIsLoading(true);
            await createShoppingItem(shoppingItem);
            showNotyf('success', 'Ajouté à la liste !');
        } catch (error) {
            showNotyf('error', 'Une erreur est survenue...');
        } finally {
            setIsLoading(false)
        }
    }

    return (
        <>
            <tr>
                <td>
                    {isEdit ? <IngredientEdit setIngredientsToShow={setIngredientsToShow} ingredient={ingredient} setIsEdit={setIsEdit} setIsLoading={setIsLoading}/> : ingredient.name}
                </td>
                <td>
                    <div className="d-flex justify-content-center">
                        <button onClick={() => onEdit(ingredient)} className="btn btn-sm btn-secondary bg-gradient me-2" data-bs-toggle="tooltip" data-bs-title="Modifier">
                            <i className="fa-solid fa-pen-to-square"></i>
                        </button>

                        <button onClick={() => onDelete(ingredient)} className="btn btn-sm btn-danger me-2 bg-gradient" data-bs-toggle="tooltip" data-bs-title="Supprimer">
                            <i className="fa-solid fa-trash"></i>
                        </button>

                        <button onClick={() => addToShoppingList(ingredient)} type="button" className="btn btn-sm btn-success bg-gradient" data-bs-toggle="tooltip" data-bs-title="Ajouter à la liste de course">
                            <i className="fa-solid fa-list"></i>
                        </button>
                    </div>
                </td>
            </tr>
        </>

    )
}

function IngredientEdit({ingredient, setIsEdit, setIsLoading, setIngredientsToShow}) {

    const { sendRequest, data, error} = useApiRequest();

    const onCancel = () => {
        setIsEdit(false);
    }

    const onSubmit = async (e, ingredient) => {
        e.preventDefault();
        const updatedIngredient = { ...ingredient, name: e.currentTarget.ingredient.value }
        setIsLoading(true);
        await sendRequest(updateIngredient, updatedIngredient)
        setIsLoading(false)
        setIsEdit(false)

        if (error) {
            showNotyf('error', error.toString())
        } else {
            showNotyf('success', 'Ingrédient modifié')

            setIngredientsToShow(ingredientList => {
                return ingredientList.map(currentIngredient => {
                    if (currentIngredient.id === updatedIngredient.id) {
                        return updatedIngredient;
                    }
                    return currentIngredient;
                })
            })
        }
    }

    return (
        <form onSubmit={(e) => onSubmit(e, ingredient)}>
            <div className="input-group">
                <input name="ingredient" type="text" className="form-control form-control-sm" defaultValue={ingredient.name}/>
                <button type="submit" className="btn btn-sm btn-outline-success bg-gradient">
                    <i className="fa-solid fa-check"></i>
                </button>
                <button onClick={onCancel} type="button" className="btn btn-sm btn-outline-danger bg-gradient">
                    <i className="fa-solid fa-xmark"></i>
                </button>
            </div>


        </form>
    )
}
