import {useNavigate} from "react-router-dom";
import {useAuth} from "../hooks/useAuth.jsx";
import {useEffect} from "react";


export default function Login() {
    const navigate = useNavigate();
    const { isAuth, login, error} = useAuth();

    useEffect(() => {
        if (isAuth) {
            navigate('/');
        }
    }, [isAuth]);

    const handleSubmit = async (e) => {
        e.preventDefault();

        const formData = new FormData(e.currentTarget);
        const username = formData.get('username')?.toString();
        const password = formData.get('password')?.toString();
        await login(username, password);

    }

    return (
        <div className="row">
            <div className="col-12">
                <h1 className="text-center">Connexion</h1>
                { error &&
                    <div className="alert alert-danger text-center">{error}</div>
                }
                <form method="POST" onSubmit={handleSubmit}>
                    <div className="row align-items-end">
                        <div className="col-md-5 col-12">
                            <label htmlFor="username">Nom d'utilisateur</label>
                            <input className="form-control" type="text" name="username" id="username"/>
                        </div>
                        <div className="col-12 col-md-5">
                            <label htmlFor="password">Mot de passe</label>
                            <input className="form-control" type="password" name="password" id="password"/>
                        </div>
                        <div className="col-md-2 mt-3">
                            <button className="btn btn-primary w-100" type="submit">Connexion</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}
