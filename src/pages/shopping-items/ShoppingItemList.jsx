import {useLoaderData} from "react-router-dom";
import {useId, useState} from "react";
import {deleteShoppingItem, updateShoppingItem} from "../../functions/api/shoppingList.js";
import {showNotyf} from "../../functions/notyf.js";
import {useApiRequest} from "../../hooks/useApiRequest.jsx";
import {Loader} from "../../components/Loader/Loader.jsx";

export function ShoppingItemList() {
    const shoppingListItems = useLoaderData()
    const {loading, sendRequest, error} = useApiRequest();

    const [list, setList] = useState(shoppingListItems);
    const handleSwitch = async (e, id) => {
        const item = list.find(item => item.id === id)

        item.isActive = !item.isActive;
        await updateShoppingItem(item);
        setList([...list]);
    }

    const onDelete = async (id) => {
        await sendRequest(deleteShoppingItem, id);
        const modifiedList = list.filter(item => item.id !== id);
        showNotyf('success', 'Élément supprimé avec succès !')
        setList([...modifiedList]);
    }

    const itemsElements = list.map(item => {
        return <tr className={item.isActive ? "table-success" : "table-warning"} key={item.id}>
            <td>{item.name}</td>
            <td>
                <div className="form-check form-switch d-flex justify-content-center">
                    <input onChange={(e) => handleSwitch(e, item.id)} checked={item.isActive} type="checkbox" role="switch" className="form-check-input switch-task-is-finished"/>
                </div>
            </td>
            <td className="text-center">
                <button onClick={() => onDelete(item.id)} className="btn btn-sm btn-danger bg-gradient"><i className="fa-solid fa-trash"></i></button>
            </td>
        </tr>
    })

    return (
        <>
            {loading && <Loader/>}
            <header>
                <h1>Lise de course</h1>
            </header>
            <hr/>

            <section className="container">
                <div className="row">
                    {itemsElements.length > 0 ?
                        <table className="table">
                            <tbody>
                            {itemsElements}
                            </tbody>
                        </table>
                        :
                        <h3 className="text-center">Aucun élément</h3>
                    }

                </div>
            </section>
        </>
    )
}
