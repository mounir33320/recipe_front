export function getToken() {
    return localStorage.getItem('token');
}

export function setToken(token) {
    return localStorage.setItem('token', token);
}

export function hasToken() {
    return !!getToken() && !!getRefreshToken();
}

export function getRefreshToken() {
    return localStorage.getItem('refresh_token');
}

export function setRefreshToken(refreshToken) {
    return localStorage.setItem('refresh_token', refreshToken);
}

export function getUserByToken() {
    return JSON.parse(atob(getToken().split('.')[1]));
}
