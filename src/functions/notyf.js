import {Notyf} from "notyf";

export const showNotyf = (type, message, duration = 3000) => {
    const notyf = new Notyf();
    notyf.open({
        type: type,
        message: message,
        position: {
            x: "right",
            y: "top"
        },
        duration: duration
    })
}
