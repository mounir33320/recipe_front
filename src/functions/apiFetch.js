import {ApiError} from "../router.jsx";
import getApiUrl from "./getApiUrl.js";
import {getRefreshToken, getToken, setRefreshToken, setToken} from "./tokenAuth.js";

export async function apiFetch(url, options = {}) {
    options = {
        method: 'GET',
        credentials: "include",
        ...options,
        headers: {
            'Content-Type': 'application/json',
            ...options.headers
        }
    }

    const response = await fetch(url, options);
    if (response.ok) {
        if (response.status === 204) {
            return true;
        }
        return await response.json();
    }

    if (response.status === 401 && !!getRefreshToken()) {
        const responseRefreshToken = await fetch(`${getApiUrl()}/api/token/refresh`, {
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify({
                refresh_token: getRefreshToken()
            })
        })

        if (responseRefreshToken.ok) {
            const result = await responseRefreshToken.json();
            setToken(result.token);
            setRefreshToken(result.refresh_token);

            const response = await fetch(url, {
                ...options,
                headers: {
                    ...options.headers,
                    'Authorization': 'Bearer ' + getToken(),
                }
            });

            if (response.ok) {
                if (response.status === 204) {
                    return true;
                }
                return await response.json();
            }
        }
    }
    localStorage.clear();

    throw new ApiError(response.status, await response.error);
}
