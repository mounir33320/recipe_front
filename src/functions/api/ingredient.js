import {apiFetch} from "../apiFetch.js";
import getApiUrl from "../getApiUrl.js";
import {getToken} from "../tokenAuth.js";

export const getAllIngredients = async () => {
    return await apiFetch(`${getApiUrl()}/api/ingredients`, {
        headers: {
            'Authorization': 'Bearer ' + getToken()
        }
    });
}

export const getIngredientById = async (id) => {
    return await apiFetch(`${getApiUrl()}/api/ingredients/${id}`, {
        headers: {
            'Authorization': 'Bearer ' + getToken()
        }
    });
}

export const createIngredient = async (ingredient) => {
    return await apiFetch(`${getApiUrl()}/api/ingredients`, {
        method: 'POST',
        body: JSON.stringify(ingredient),
        headers: {
            'Authorization': 'Bearer ' + getToken()
        }
    });
}

export const updateIngredient = async (ingredient) => {
    return await apiFetch(`${getApiUrl()}/api/ingredients/${ingredient.id}`, {
        method: 'PUT',
        body: JSON.stringify(ingredient),
        headers: {
            'Authorization': 'Bearer ' + getToken()
        }
    });
}

export const deleteIngredient = async (id) => {
    return await apiFetch(`${getApiUrl()}/api/ingredients/${id}`, {
        method: 'DELETE',
        headers: {
            'Authorization': 'Bearer ' + getToken()
        }
    });
}
