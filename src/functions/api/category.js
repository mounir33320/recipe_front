import {apiFetch} from "../apiFetch.js";
import getApiUrl from "../getApiUrl.js";
import {getToken} from "../tokenAuth.js";

export const getAllCategories = async () => {
    return await apiFetch(`${getApiUrl()}/api/categories`, {
        headers: {
            'Authorization': 'Bearer ' + getToken()
        }
    });
}

export const getCategoryById = async (id) => {
    return await apiFetch(`${getApiUrl()}/api/categories/${id}`, {
        headers: {
            'Authorization': 'Bearer ' + getToken()
        }
    });
}

export const createCategory = async (category) => {
    return await apiFetch(`${getApiUrl()}/api/category`, {
        method: 'POST',
        body: JSON.stringify(category),
        headers: {
            'Authorization': 'Bearer ' + getToken()
        }
    });
}

export const updateCategory = async (category) => {
    return await apiFetch(`${getApiUrl()}/api/categories/${category.id}`, {
        method: 'PUT',
        body: JSON.stringify(category),
        headers: {
            'Authorization': 'Bearer ' + getToken()
        }
    });
}

export const deleteCategory = async (id) => {
    return await apiFetch(`${getApiUrl()}/api/categories/${id}`, {
        method: 'DELETE',
        headers: {
            'Authorization': 'Bearer ' + getToken()
        }
    });
}
