import {apiFetch} from "../apiFetch.js";
import getApiUrl from "../getApiUrl.js";
import {getToken} from "../tokenAuth.js";

export const getAllShoppingItems = async () => {
    return await apiFetch(`${getApiUrl()}/api/shopping-items`, {
        headers: {
            'Authorization': 'Bearer ' + getToken()
        }
    });
}

export const createShoppingItem = async (shoppingItem) => {
    return await apiFetch(`${getApiUrl()}/api/shopping-items`, {
        method: 'POST',
        body: JSON.stringify(shoppingItem),
        headers: {
            'Authorization': 'Bearer ' + getToken()
        }
    });
}

export const updateShoppingItem = async (shoppingItem) => {
    return await apiFetch(`${getApiUrl()}/api/shopping-items/${shoppingItem.id}`, {
        method: 'PUT',
        body: JSON.stringify(shoppingItem),
        headers: {
            'Authorization': 'Bearer ' + getToken()
        }
    });
}

export const deleteShoppingItem = async (id) => {
    return await apiFetch(`${getApiUrl()}/api/shopping-items/${id}`, {
        method: 'DELETE',
        headers: {
            'Authorization': 'Bearer ' + getToken()
        }
    });
}
