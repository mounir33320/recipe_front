import {apiFetch} from "../apiFetch.js";
import getApiUrl from "../getApiUrl.js";
import {getToken} from "../tokenAuth.js";

export const getAllRecipes = async () => {
    return await apiFetch(`${getApiUrl()}/api/recipes`, {
        headers: {
            'Authorization': 'Bearer ' + getToken()
        }
    });
}

export const getRecipeById = async (id) => {
    return await apiFetch(`${getApiUrl()}/api/recipes/${id}`, {
        headers: {
            'Authorization': 'Bearer ' + getToken()
        }
    });
}

export const createRecipe = async (recipe) => {
    return await apiFetch(`${getApiUrl()}/api/recipes`, {
        method: 'POST',
        body: JSON.stringify(recipe),
        headers: {
            'Authorization': 'Bearer ' + getToken()
        }
    })
}

export const updateRecipe = async (recipe) => {
    return await apiFetch(`${getApiUrl()}/api/recipes/${recipe.id}`, {
        method: 'PUT',
        body: JSON.stringify(recipe),
        headers: {
            'Authorization': 'Bearer ' + getToken()
        }
    })
}

export const deleteRecipe = async (id) => {
    return await apiFetch(`${getApiUrl()}/api/recipes/${id}`, {
        method: 'DELETE',
        headers: {
            'Authorization': 'Bearer ' + getToken()
        }
    })
}

