import {apiFetch} from "../apiFetch.js";
import getApiUrl from "../getApiUrl.js";
import {getToken} from "../tokenAuth.js";

export const getAllDays = async () => {
    return await apiFetch(`${getApiUrl()}/api/days`, {
        headers: {
            'Authorization': 'Bearer ' + getToken()
        }
    });
}

export const updateDay = async (data) => {
    return await apiFetch(`${getApiUrl()}/api/days/${data.id}`, {
        method: 'PUT',
        body: JSON.stringify(data),
        headers: {
            'Authorization': 'Bearer ' + getToken()
        }
    })
}
