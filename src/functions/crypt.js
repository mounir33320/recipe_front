import CryptoJS from "crypto-js/crypto-js.js";
import {secret} from "./secret.js";

export const encrypt = (data) => {
    return CryptoJS.AES.encrypt(data, secret).toString();
}

export const decrypt = (data) => {
    if (!data) {
        return null;
    }
    let decrypted = CryptoJS.AES.decrypt(data, secret)
    decrypted = decrypted.toString(CryptoJS.enc.Utf8);

    if (!decrypted) {
        return null;
    }
    return decrypted
}

